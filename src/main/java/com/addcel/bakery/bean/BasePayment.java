package com.addcel.bakery.bean;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown=true)
public class BasePayment {

    private long idUser;

    private int idCard;

    private double cargo;

    private double comision;

    private String concepto;

    private String referencia;

    private int idProveedor;

    private int idProducto;

    private String imei;

    private String modelo;

    private String software;

    private double lat;

    private double lon;

    private int establecimientoId;

    private int idAplicacion;

    private String idioma;

    private String tipoTarjeta;

}

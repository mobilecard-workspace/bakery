package com.addcel.bakery.bean;

import lombok.Data;

@Data
public class OrdenPago {

    private long idUsuario;

    private double monto;

    private String concepto;
}

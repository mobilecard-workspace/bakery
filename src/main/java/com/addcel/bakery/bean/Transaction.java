package com.addcel.bakery.bean;

import lombok.Data;

@Data
public class Transaction {

    private long idBitacora;

    private int code;

    private String message;

    private String order;

    private String client;

}

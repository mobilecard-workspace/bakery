package com.addcel.bakery.bean.bakery;

import lombok.Data;

@Data
public class CardRequest {


    /**
     * customer_ext_id : 12345678A
     * card_holder : John Doe
     * card_pan : 4111111111111111
     * card_expiry_year : 19
     * card_expiry_month : 12
     * card_cvv : 123
     * signature : c6f00988430dbc8e83a7bc7ab5256346
     * validate : true
     * service : AS12F4E3-AS12F4E3-AS12F4E3-
     * additional : card12345
     * url_post :
     */

    private String customer_ext_id;
    private String card_holder;
    private String card_pan;
    private String card_expiry_year;
    private String card_expiry_month;
    private String card_cvv;
    private String signature;
    private boolean validate;
    private String service;
    private String additional;
    private String url_post;

}

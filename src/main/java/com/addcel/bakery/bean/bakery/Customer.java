package com.addcel.bakery.bean.bakery;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class Customer {

    @JsonProperty("customer_ext_id")
    private String customerExtId;

    private String signature;
}
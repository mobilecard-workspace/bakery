package com.addcel.bakery.bean.bakery;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class CustomerResponse {

    @JsonProperty("external_id")
    private String externalId;

    @JsonProperty("token")
    private String token;
}
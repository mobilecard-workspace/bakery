package com.addcel.bakery.bean.bakery;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class Deposito {

    @JsonProperty("order_uuid")
    private String orderUuid;

    @JsonProperty("source_uuid")
    private String sourceUuid;

    private String signature;
}

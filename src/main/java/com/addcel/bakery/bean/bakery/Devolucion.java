package com.addcel.bakery.bean.bakery;

import lombok.Data;

@Data
public class Devolucion {

    private String signature;
    private String order_uuid;

}

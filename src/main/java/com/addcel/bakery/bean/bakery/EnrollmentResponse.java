package com.addcel.bakery.bean.bakery;

import com.addcel.bakery.bean.BaseResponse;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class EnrollmentResponse extends BaseResponse {

    @JsonProperty("current_time")
    private String currentTime;

    @JsonProperty("Customer")
    private CustomerResponse customer;

}
package com.addcel.bakery.bean.bakery;

import lombok.Data;

@Data
public class OrdenPagoRequest {

    private String operative;

    private String template_uuid;

    private String signature;

    private String description;

    private String source_uuid;

    private String service;

    private String url_ok;

    private String customer_ext_id;

    private String url_post;

    private String url_ko;

    private String additional;

    private double amount;
}

package com.addcel.bakery.bean.bakery;

import com.addcel.bakery.bean.BaseResponse;
import lombok.Data;

@Data
public class OrdenPagoResponse extends BaseResponse {

    private String current_time;

    private OrderEntity order;

    private Client client;

}

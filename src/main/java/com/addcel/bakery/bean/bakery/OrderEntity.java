package com.addcel.bakery.bean.bakery;

import lombok.Data;

import java.util.List;

@Data
public class OrderEntity {

    private double amount;
    private String antifraud;
    private String created;
    private String additional;
    private String ip;
    private List<TransactionsEntity> transactions;
    private String uuid;
    private String created_from_client_timezone;
    private String token;
    private String service;
    private boolean paid;
    private boolean safe;
    private String currency;
    private int refunded;
    private String customer;
}

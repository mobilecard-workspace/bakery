package com.addcel.bakery.bean.bakery;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class Pago {

    @JsonProperty("signature")
    private String signature;

    @JsonProperty("customer_ip")
    private String customer_ip;

    @JsonProperty("order_uuid")
    private String order_uuid;

    @JsonProperty("card_uuid")
    private String card_uuid;
}

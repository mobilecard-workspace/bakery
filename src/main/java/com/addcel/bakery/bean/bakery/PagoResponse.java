package com.addcel.bakery.bean.bakery;

import com.addcel.bakery.bean.BaseResponse;
import lombok.Data;


@Data
public class PagoResponse extends BaseResponse {

    private Client client;

    private String current_time;

    private OrderEntity order;

}

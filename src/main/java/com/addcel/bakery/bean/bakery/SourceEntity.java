package com.addcel.bakery.bean.bakery;

import lombok.Data;

@Data
public class SourceEntity {

    private String country;
    private String last4;
    private String bin;
    private String additional;
    private String expire_month;
    private String holder;
    private String type;
    private String uuid;
    private String token;
    private String bank;
    private String expire_year;
    private String brand;
    private String object;
    private String prepaid;
}

package com.addcel.bakery.bean.bakery;

import com.addcel.bakery.bean.bakery.SourceEntity;
import lombok.Data;

@Data
public class TransactionsEntity {

    private String authorization;
    private int amount;
    private String created;
    private SourceEntity source;
    private String error;
    private String uuid;
    private String operative;
    private String created_from_client_timezone;
    private String status;
}

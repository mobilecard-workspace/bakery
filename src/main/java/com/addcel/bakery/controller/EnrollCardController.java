package com.addcel.bakery.controller;

import com.addcel.bakery.bean.BaseResponse;
import com.addcel.bakery.services.PBClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class EnrollCardController {

    @Autowired
    private PBClientService service;

    private final static String PATH_ENROLL_CARD = "/{idApp}/{idPais}/{idioma}/{idUsuario}/enroll/card";

    @RequestMapping(value = PATH_ENROLL_CARD, method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    public ResponseEntity<Object> verifyEnrollmentCardholder(@PathVariable Integer idApp,
                                                             @PathVariable Integer idPais,
                                                             @PathVariable String idioma,
                                                             @PathVariable long idUsuario) {
        ResponseEntity<Object> response = null;
        try {
            response = new ResponseEntity<>(service.enrolamientoCliente(idUsuario), HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            BaseResponse error = new BaseResponse();
            error.setCode(-10);
            error.setMessage("ERROR: "+e.getLocalizedMessage());
            response = new ResponseEntity<>(error, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return response;
    }
}

package com.addcel.bakery.controller;

import com.addcel.bakery.bean.BasePayment;
import com.addcel.bakery.bean.BaseResponse;
import com.addcel.bakery.bean.OrdenPago;
import com.addcel.bakery.bean.bakery.Deposito;
import com.addcel.bakery.services.PagosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class PagosController {

    @Autowired
    private PagosService service;

    private final static String PATH_ABONO_TARJETA = "/{idApp}/{idPais}/{idioma}/abonar/saldo";

    private final static String PATH_GENERA_ORDEN_PAGO = "/{idApp}/{idPais}/{idioma}/genera/ordenPago";

    private final static String PATH_DEVOLUCION_PAGO = "/{idApp}/{idPais}/{idioma}/devolucion/pago";

    private final static String PATH_PROCESA_PAGO = "/{idApp}/{idPais}/{idioma}/procesa/pago";

    //@RequestMapping(value = PATH_PROCESA_PAGO, method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @PostMapping(value = PATH_PROCESA_PAGO)
    public ResponseEntity<Object> procesaPago(@PathVariable Integer idApp,
                                               @PathVariable Integer idPais,
                                               @PathVariable String idioma,
                                               @RequestBody BasePayment pago) {
        ResponseEntity<Object> response = null;
        try {
            response = new ResponseEntity<>(service.realizarpago(idApp, idPais, idioma, pago), HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            BaseResponse error = new BaseResponse();
            error.setCode(-10);
            error.setMessage("ERROR: "+e.getLocalizedMessage());
            response = new ResponseEntity<>(error, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return response;
    }

    //@RequestMapping(value = PATH_ABONO_TARJETA, method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @PostMapping(value = PATH_ABONO_TARJETA)
    public ResponseEntity<Object> abonoTarjeta(@PathVariable Integer idApp,
                                                @PathVariable Integer idPais,
                                                @PathVariable String idioma,
                                                @RequestBody Deposito deposito) {
        ResponseEntity<Object> response = null;
        try {
            response = new ResponseEntity<>(service.depositoTarjeta(deposito), HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            BaseResponse error = new BaseResponse();
            error.setCode(-10);
            error.setMessage("ERROR: "+e.getLocalizedMessage());
            response = new ResponseEntity<>(error, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return response;
    }

    //@RequestMapping(value = PATH_GENERA_ORDEN_PAGO, method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @PostMapping(value = PATH_GENERA_ORDEN_PAGO)
    public ResponseEntity<Object> generarOrdenPago(@PathVariable Integer idApp,
                                                   @PathVariable Integer idPais,
                                                   @PathVariable String idioma,
                                                   @RequestBody OrdenPago request) {
        ResponseEntity<Object> response = null;
        try {
            response = new ResponseEntity<>(service.generaOrdenPago(request.getIdUsuario(), request.getMonto(),
                    request.getConcepto(), ""), HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            BaseResponse error = new BaseResponse();
            error.setCode(-10);
            error.setMessage("ERROR: "+e.getLocalizedMessage());
            response = new ResponseEntity<>(error, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return response;
    }

    @GetMapping(value = PATH_DEVOLUCION_PAGO)
    public ResponseEntity<Object> devolucionPago(@PathVariable Integer idApp,
                                                   @PathVariable Integer idPais,
                                                   @PathVariable String idioma,
                                                       @RequestParam String orderUuid) {
        ResponseEntity<Object> response = null;
        try {
            response = new ResponseEntity<>(service.devolucion(orderUuid), HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            BaseResponse error = new BaseResponse();
            error.setCode(-10);
            error.setMessage("ERROR: "+e.getLocalizedMessage());
            response = new ResponseEntity<>(error, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return response;
    }

}

package com.addcel.bakery.controller;

import com.addcel.bakery.bean.BaseResponse;
import com.addcel.bakery.bean.bakery.PagoResponse;
import com.addcel.bakery.services.PBClientService;
import com.addcel.bakery.services.PBClientServiceImpl;
import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class PagosRespuestaController {

    private static final Logger LOGGER = LoggerFactory.getLogger(PBClientServiceImpl.class);

    @Autowired
    private PBClientService service;

    private final static String PATH_PAYMENT_BAKERY_RESULT = "/pb/result";

    private final static String PATH_PAYMENT_BAKERY_SUCCESS = "/pb/success";

    private final static String PATH_PAYMENT_BAKERY_ERROR = "/pb/error";

    @Autowired
    private Gson GSON;

    @RequestMapping(value = PATH_PAYMENT_BAKERY_RESULT, method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    public ResponseEntity<Object> paymentResult(@RequestBody PagoResponse responsePago) {
        ResponseEntity<Object> response = null;
        try {
            LOGGER.info("RESULT DE PAYMENT BAKERY - {}",  GSON.toJson(responsePago));
        } catch (Exception e) {
            e.printStackTrace();
            BaseResponse error = new BaseResponse();
            error.setCode(-10);
            error.setMessage("ERROR: "+e.getLocalizedMessage());
            response = new ResponseEntity<>(error, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return response;
    }

    @RequestMapping(value = PATH_PAYMENT_BAKERY_SUCCESS, method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    public ResponseEntity<Object> paymentSuccess(@RequestBody PagoResponse responsePago) {
        ResponseEntity<Object> response = null;
        try {
            LOGGER.info("SUCCESS  DE PAYMENT BAKERY - {}",  GSON.toJson(responsePago));
        } catch (Exception e) {
            e.printStackTrace();
            BaseResponse error = new BaseResponse();
            error.setCode(-10);
            error.setMessage("ERROR: "+e.getLocalizedMessage());
            response = new ResponseEntity<>(error, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return response;
    }

    @RequestMapping(value = PATH_PAYMENT_BAKERY_ERROR, method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    public ResponseEntity<Object> paymentError(@RequestBody PagoResponse responsePago) {
        ResponseEntity<Object> response = null;
        try {
            LOGGER.info("ERROR DE PAYMENT BAKERY - {}",  GSON.toJson(responsePago));
        } catch (Exception e) {
            e.printStackTrace();
            BaseResponse error = new BaseResponse();
            error.setCode(-10);
            error.setMessage("ERROR: "+e.getLocalizedMessage());
            response = new ResponseEntity<>(error, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return response;
    }

}

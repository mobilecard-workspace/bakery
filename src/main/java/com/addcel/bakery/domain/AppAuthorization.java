package com.addcel.bakery.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "APP_AUTHORIZATION")
public class AppAuthorization {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id_app_auth;
	
	@Column(name = "id_application", nullable = false)
	private String idApplication;
		
	@Column(name = "username", nullable = false)
	private String username;
	
	@Column(name = "password", nullable = false)
	private String password;
	
	@Column(name = "activo", nullable = false)
	private char activo;
	
}

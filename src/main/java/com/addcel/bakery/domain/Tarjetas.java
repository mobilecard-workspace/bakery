package com.addcel.bakery.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "tarjetas_usuario")
public class Tarjetas {

    @Id
    @Column(name = "idtarjetasusuario")
    private Integer idTarjeta;

    @Column(name = "id_aplicacion")
    private Integer idApp;

    @Column(name = "numerotarjeta")
    private String pan;

    @Column(name = "vigencia")
    private String expDate;

    @Column(name = "ct")
    private String ct;

    @Column(name = "estado")
    private String estado;

    @Column(name = "uuid")
    private String uuid;


}

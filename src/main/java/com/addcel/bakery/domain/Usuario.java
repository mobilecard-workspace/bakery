package com.addcel.bakery.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "t_usuarios")
public class Usuario {

    @Id
    @Column(name = "id_usuario")
    private long idUsuario;

    @Column(name = "usr_login")
    private String login;

    @Column(name = "usr_pwd")
    private String password;

    @Column(name = "usr_telefono")
    private String numCelular;

    @Column(name = "usr_nombre")
    private String nombre;

    @Column(name = "usr_apellido")
    private String apellidoP;

    @Column(name = "usr_materno")
    private String apellidoM;

    @Column(name = "email")
    private String email;

    @Column(name = "id_usr_status")
    private int status;

    @Column(name = "pb_token")
    private String customerExtId;

}

package com.addcel.bakery.repositories;

import com.addcel.bakery.domain.AppAuthorization;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface AppAuthorizationRepository extends CrudRepository<AppAuthorization, Long> {

	public AppAuthorization findByUsernameAndActivo(@Param("username") String username,
                                                    @Param("activo") char activo);
	
	public List<AppAuthorization> findByActivo(@Param("activo") char activo);
	
}

package com.addcel.bakery.repositories;

import com.addcel.bakery.domain.Transaction;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface AuthorizationRepository extends CrudRepository<Transaction, Long> {


    @Query(value = "SELECT valida_transaccion_bakery(:nidusuario, :nidaplicacion, :nidioma, :nid_producto, " +
            ":nbit_card_id, :nimei, :nproveedores)",
            nativeQuery = true)
    String validaTransaccion(@Param("nidusuario") long idUsuario,   @Param("nidaplicacion") int idApp,
                             @Param("nidioma") String idioma,       @Param("nid_producto") int idProducto,
                             @Param("nbit_card_id") int idTarjeta,  @Param("nimei") String imei,
                             @Param("nproveedores") String proveedores);

    @Query(value = "SELECT insertatransaccion_payment_bakery(:nidusuario, :nidaplicacion, :nidioma, "+
            ":nid_proveedor, :nid_integrador, :nid_producto, :nbit_concepto, :nreferencia, :nbit_cargo, :ncomision, " +
            ":nbit_card_id, :ncardtype, :nimei, :nsoftware, :nmodelo, :nlat, :nlon)", nativeQuery = true)
    String insertatransaccion_payment_bakery(@Param("nidusuario") long idUsuario,
                                         @Param("nidaplicacion") int idApp,         @Param("nidioma") String idioma,
                                         @Param("nid_proveedor") int idProveedor,   @Param("nid_integrador") int idIntegrador,
                                         @Param("nid_producto") int idProducto,     @Param("nbit_concepto") String concepto,
                                         @Param("nreferencia") String referencia,   @Param("nbit_cargo") double cargo,
                                         @Param("ncomision") double comision,       @Param("nbit_card_id") int idTarjeta,
                                         @Param("ncardtype") String tipoTarjeta,    @Param("nimei") String imei,
                                         @Param("nsoftware") String software,       @Param("nmodelo") String modelo,
                                         @Param("nlat") double lat,                 @Param("nlon") double lon);

    @Query(value = "SELECT actualiza_order_payment_bakery(:idTransaccion, :ncode, :nmessage, :norder, " +
            " :nclient)", nativeQuery = true)
    String actualiza_order_payment_bakery(@Param("idTransaccion") long idTransaccion, @Param("ncode") int code,
                                          @Param("nmessage") String message, @Param("norder") String order,
                                          @Param("nclient") String client);


    @Query(value = "SELECT actualizatransaccion_payment_bakery(:idTransaccion, :status, :message, :opId, " +
            " :txnISOCode, :authNumber, :ticketUrl, :maskedPAN)", nativeQuery = true)
    String actualizatransaccion_billpocket(@Param("idTransaccion") long idTransaccion, @Param("status") int status,
                                           @Param("message") String message, @Param("opId") long opId,
                                           @Param("txnISOCode") String txnISOCode, @Param("authNumber") String authNumber,
                                           @Param("ticketUrl") String ticketUrl, @Param("maskedPAN") String maskedPAN);

    Optional<Transaction> findByIdBitacora(Long idTransaccion);
}

package com.addcel.bakery.repositories;

import com.addcel.bakery.domain.Tarjetas;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface TarjetasRepository extends CrudRepository<Tarjetas, Long> {

    Optional<Tarjetas> findByIdTarjeta(Integer idTarjeta);

}


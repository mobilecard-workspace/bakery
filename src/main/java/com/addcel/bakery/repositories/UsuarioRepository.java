package com.addcel.bakery.repositories;

import com.addcel.bakery.domain.Usuario;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface UsuarioRepository extends CrudRepository<Usuario, Long> {

    Optional<Usuario> findByIdUsuario(Long idusuario);

}


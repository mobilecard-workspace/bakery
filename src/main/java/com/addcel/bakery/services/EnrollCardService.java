package com.addcel.bakery.services;

import com.addcel.bakery.bean.bakery.EnrollmentResponse;

public interface EnrollCardService {

    EnrollmentResponse enrolment(long idUsuario);

}

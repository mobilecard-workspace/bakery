package com.addcel.bakery.services;

import com.addcel.bakery.bean.bakery.EnrollmentResponse;
import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EnrollCardServiceImpl implements EnrollCardService {

    private static final Logger LOGGER = LoggerFactory.getLogger(EnrollCardServiceImpl.class);

    @Autowired
    private PBClientService clientService;

    private Gson GSON = new Gson();

    @Override
    public EnrollmentResponse enrolment(long idUsuario) {
        EnrollmentResponse response = null;
        String signature = null;
        try{
            response = clientService.enrolamientoCliente(idUsuario);
        } catch (Exception e){
            e.printStackTrace();
        }
        LOGGER.info("RESPUESTA BP: {}", GSON.toJson(response));
        return response;
    }

}

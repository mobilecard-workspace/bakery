package com.addcel.bakery.services;


import com.addcel.bakery.bean.bakery.*;

public interface PBClientService {

    EnrollmentResponse enrolamientoCliente(long idUsuario);

    OrdenPagoResponse generaOrdenPago(OrdenPagoRequest request);

    OrdenPagoResponse generaOrdenPago(long customerExtId, double amount, long idBitacora,
                                      String templateUuid, String concepto, String sourceId);

    PagoResponse realizarPago(String orderUuid, String cardUuid);

    DevolucionPago devolucionPago(Devolucion devolucion);

    PagoResponse depositoTarjeta(Deposito deposito);

    CardResponse enrolamientoTarjeta(String pan, String expDate, String ct,
                                            String customerExtId, String nombre);

}

package com.addcel.bakery.services;

import com.addcel.bakery.bean.bakery.*;
import com.addcel.utils.AddcelCrypto;
import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class PBClientServiceImpl implements PBClientService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PBClientServiceImpl.class);

    @Autowired
    private RestTemplate restTemplate;

    // URL QA
    //private static final String URL_PB = "https://api.paylands.com/v1/sandbox/";

    // URL PROD
    private static final String URL_PB = "https://api.paylands.com/v1/";

    private static final String URL_PB_ABONO = "payment/payout";

    private static final String URL_PB_ORDEN_PAGO = "payment";

    private static final String URL_PB_ENROLL_CLIENT = "customer";

    private static final String URL_PB_ENROLL_CARD = "payment-method/card";

    private static final String URL_PB_PAYMENT_DIRECT = "payment/direct";

    private static final String URL_PB_PAYMENT_REFUND = "payment/refund";

    // SIGNATURE_QA
    //private static final String PB_SIGNATURE = "cnBczJfKquSV4O9ne4WADidG";
    // SIGNATURE PROD
    //private static final String PB_SIGNATURE = "j5GcKJbzXeJeybQ1zqMebhXl";
    // PAYMENT BAKERY SIGNATURE
    private static final String PB_SIGNATURE = "ICl7QxKgqGD0JcAv7UFrUpKY";

    // API-KEY_QA
    //private static final String PB_BASIC_AUTH = "Basic NjJFNDU0Qzk2QTA5NDk1OEE4NzZENUVEMTVBMzQzQTM6";
    // API-KEY PROD
    //private static final String PB_BASIC_AUTH = "Basic ZDYyZDZmNTBhYjFmNDFjOTllNmU5OWJjMTkwZGI1Y2Y6";
    // API KEY - PAYMENT BAKERY
    private static final String PB_BASIC_AUTH = "Basic QzA5MzlBMDk0Q0VENDFGOEIxQkVFNkIyQUYzQTM2NTc6";


    //PB_SERVICE_PROD
    //private static final String PB_SERVICE = "1F6BBD6A-91E2-4248-A82F-8EE0F21A7B59";
    //PAYMENT BAKERY UUID
    private static final String PB_SERVICE = "5D05EB4D-40A7-47CF-BCE3-7782A027A1D1";
    //private static final String PB_SERVICE = "5D05EB4D-40A7-47CF-BCE3-7782A027A1D1";
    //PB_SERVICE_QA
    //private static final String PB_SERVICE = "2FE08235-F916-4418-B0A2-0D094D7C8742";

    //@Autowired
    //private OrderRepository orderRepository;

    @Autowired
    private Gson GSON;

    @Override
    public EnrollmentResponse enrolamientoCliente(long idUsuario) {
        Customer customer = new Customer();
        EnrollmentResponse response = null;
        try {
            customer.setCustomerExtId(String.valueOf(idUsuario));
            customer.setSignature(PB_SIGNATURE);
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.set("Authorization", PB_BASIC_AUTH);
            HttpEntity<Customer> request = new HttpEntity<>(customer, headers);
            ResponseEntity<EnrollmentResponse> respEnroll = restTemplate.exchange(URL_PB+URL_PB_ENROLL_CLIENT,
                    HttpMethod.POST, request, EnrollmentResponse.class);
            response = respEnroll.getBody();
            LOGGER.info("RESPUESTA PB - {}", respEnroll.getBody());
        } catch (Exception e){
            e.printStackTrace();
            response = new EnrollmentResponse();
            response.setCode(-100);
            response.setMessage("No fue posible realizar el enrolamiento con el cliente. Codigo error: PB-1100");
        }
        LOGGER.info("RESPUESTA DE ENROLAMIENTO DE CLIENTE - {}", GSON.toJson(response));
        return response;
    }

    @Override
    public CardResponse enrolamientoTarjeta(String pan, String expDate, String ct,
                                                  String customerExtId, String nombre) {
        CardRequest cardRequest = new CardRequest();
        CardResponse response = null;
        String mes = null;
        String año = null;
        String card = null;
        try {
            card = AddcelCrypto.decryptTarjeta(expDate);
            año = card.substring(3, card.length());
            mes = card.substring(0, 2);
            cardRequest.setCustomer_ext_id(customerExtId);
            cardRequest.setCard_holder(nombre);
            cardRequest.setCard_pan(AddcelCrypto.decryptTarjeta(pan));
            cardRequest.setCard_expiry_year(año);
            cardRequest.setCard_expiry_month(mes);
            cardRequest.setCard_cvv(AddcelCrypto.decryptTarjeta(ct));
            cardRequest.setSignature(PB_SIGNATURE);
            cardRequest.setValidate(false);
            cardRequest.setService(PB_SERVICE);
            cardRequest.setUrl_post("https://www.mobilecard.mx/Bakery");
            cardRequest.setAdditional(nombre);
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.set("Authorization", PB_BASIC_AUTH);

            HttpEntity<CardRequest> request = new HttpEntity<>(cardRequest, headers);
            LOGGER.info("REQUEST ENROLL CARD  PB - {}", GSON.toJson(request));
            ResponseEntity<CardResponse> respEnroll = restTemplate.exchange(URL_PB+URL_PB_ENROLL_CARD,
                    HttpMethod.POST, request, CardResponse.class);
            response = respEnroll.getBody();
            LOGGER.info("RESPUESTA PB - {}", respEnroll.getBody());
        } catch (Exception e){
            e.printStackTrace();
            response = new CardResponse();
            response.setCode(-100);
            response.setMessage("No fue posible realizar el enrolamiento de la tarjeta. Codigo error: PB-1100");
        }
        LOGGER.info("RESPUESTA DE ENROLAMIENTO DE TARJETA - {}", GSON.toJson(response));
        return response;
    }

    @Override
    public OrdenPagoResponse generaOrdenPago(OrdenPagoRequest orderRequest) {
        OrdenPagoResponse response = null;
        try {
            orderRequest.setSignature(PB_SIGNATURE);
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.set("Authorization", PB_BASIC_AUTH);
            HttpEntity<OrdenPagoRequest> request = new HttpEntity<>(orderRequest, headers);
            LOGGER.info("REQUEST CREATE ORDER PB - {}", GSON.toJson(request));
            ResponseEntity<OrdenPagoResponse> respOrdernPago = restTemplate.exchange(URL_PB+URL_PB_ORDEN_PAGO,
                    HttpMethod.POST, request, OrdenPagoResponse.class);
            response = respOrdernPago.getBody();
        } catch (Exception e){
            e.printStackTrace();
            response = new OrdenPagoResponse();
            response.setCode(-100);
            response.setMessage("No fue posible realizar la creacion de la orden. Codigo error: 1200");
        }
        LOGGER.info("RESPUESTA DE CREACION DE ORDEN - {}", GSON.toJson(response));
        return response;
    }

    @Override
    public OrdenPagoResponse generaOrdenPago(long customerExtId, double amount, long idBitacora,
                                             String templateUuid, String concepto, String sourceId) {
        OrdenPagoResponse response = null;
        OrdenPagoRequest orderRequest = new OrdenPagoRequest();
        try {
            orderRequest.setSignature(PB_SIGNATURE);
            orderRequest.setAmount(amount);
            orderRequest.setOperative("AUTHORIZATION");
            orderRequest.setCustomer_ext_id(String.valueOf(customerExtId));
            orderRequest.setAdditional(concepto);
            orderRequest.setService(PB_SERVICE);
            orderRequest.setUrl_post("https://www.mobilecard.mx/Bakery/pb/result");
            orderRequest.setUrl_ok("https://www.mobilecard.mx/Bakery/pb/success");
            orderRequest.setUrl_ko("https://www.mobilecard.mx/Bakery/pb/error");
            orderRequest.setTemplate_uuid(templateUuid);
            orderRequest.setDescription(concepto);
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.set("Authorization", PB_BASIC_AUTH);
            HttpEntity<OrdenPagoRequest> request = new HttpEntity<>(orderRequest, headers);
            LOGGER.info("REQUEST CREATE ORDER PB - {}", GSON.toJson(orderRequest));
            ResponseEntity<OrdenPagoResponse> respOrdernPago = restTemplate.exchange(URL_PB+URL_PB_ORDEN_PAGO,
                    HttpMethod.POST, request, OrdenPagoResponse.class);
            LOGGER.info("RESPONSE CREATE ORDER PB - {}", GSON.toJson(respOrdernPago));
            response = respOrdernPago.getBody();
        } catch (Exception e){
            e.printStackTrace();
            response = new OrdenPagoResponse();
            response.setCode(-100);
            response.setMessage("No fue posible realizar la creacion de la orden. Codigo error: 1200");
        }
        LOGGER.info("RESPUESTA DE CREACION DE ORDEN - {}", GSON.toJson(response));
        return response;
    }

    @Override
    public PagoResponse realizarPago(String orderUuid, String cardUuid) {
        Pago pago = new Pago();
        PagoResponse response = null;
        try {
            pago.setSignature(PB_SIGNATURE);
            pago.setCard_uuid(cardUuid);
            pago.setOrder_uuid(orderUuid);
            pago.setCustomer_ip("192.168.75.51");
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.set("Authorization", PB_BASIC_AUTH);
            HttpEntity<Pago> request = new HttpEntity<>(pago, headers);
            LOGGER.info("DATOS ENVIADOS A PAYMENT BAKERY - {}", GSON.toJson(request));
            ResponseEntity<PagoResponse> respOrdernPago = restTemplate.exchange(URL_PB+URL_PB_PAYMENT_DIRECT,
                    HttpMethod.POST, request, PagoResponse.class);
            response = respOrdernPago.getBody();
            LOGGER.info("RESPUESTA DE BAKERY - {}", GSON.toJson(respOrdernPago));
        } catch (Exception e){
            e.printStackTrace();
            response = new PagoResponse();
            response.setCode(-100);
            response.setMessage("No fue posible procesar el cobro con el autorizador. Codigo error: 1200");
        }
        LOGGER.info("RESPUESTA DE PAGO - {}", GSON.toJson(response));
        return response;
    }

    @Override
    public DevolucionPago devolucionPago(Devolucion devolucion) {
        DevolucionPago response = new DevolucionPago();
        try {
            devolucion.setSignature(PB_SIGNATURE);
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.set("Authorization", PB_BASIC_AUTH);
            HttpEntity<Devolucion> request = new HttpEntity<>(devolucion, headers);
            LOGGER.info("REQUEST ENVIADO A PAYMENT BAKERY - {}", GSON.toJson(request));
            ResponseEntity<DevolucionPago> respOrdernPago = restTemplate.exchange(URL_PB+URL_PB_PAYMENT_REFUND,
                    HttpMethod.POST, request, DevolucionPago.class);
            LOGGER.info("RESPUESTA DE PAYMENT BAKERY - {}", GSON.toJson(respOrdernPago));
            response = respOrdernPago.getBody();
        } catch (Exception e){
            e.printStackTrace();
            response.setCode(-200);
            response.setMessage("No fue posible realizar el reverso del pago. Codigo error: 1200");
        }
        LOGGER.info("RESPUESTA DE REVERSO - {}", GSON.toJson(response));
        return response;
    }

    @Override
    public PagoResponse depositoTarjeta(Deposito deposito) {
        PagoResponse response = new PagoResponse();
        try {
            deposito.setSignature(PB_SIGNATURE);
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.set("Authorization", PB_BASIC_AUTH);
            HttpEntity<Deposito> request = new HttpEntity<>(deposito, headers);
            ResponseEntity<PagoResponse> respOrdernPago = restTemplate.exchange(URL_PB+URL_PB_ABONO,
                    HttpMethod.POST, request, PagoResponse.class);
            response = respOrdernPago.getBody();
        } catch (Exception e){
            e.printStackTrace();
            response.setCode(-100);
            response.setMessage("No fue posible realizar el enrolamiento con el cliente. Codigo error: 1200");
        }
        LOGGER.info("RESPUESTA DE ENROLAMIENTO DE TARJETA - {}", GSON.toJson(response));
        return response;
    }

}

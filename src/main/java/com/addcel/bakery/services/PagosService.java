package com.addcel.bakery.services;

import com.addcel.bakery.bean.*;
import com.addcel.bakery.bean.bakery.*;

public interface PagosService {

    OrdenPagoResponse generaOrdenPago(long idUsuario, double monto, String concepto,
                                      String uuid);

    PagoResponse realizarpago(Integer idApp, Integer idPais, String idioma, BasePayment pago);

    PagoResponse depositoTarjeta(Deposito deposito);

    public DevolucionPago devolucion(String orderUuid);
}

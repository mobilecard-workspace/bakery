package com.addcel.bakery.services;

import com.addcel.bakery.bean.*;
import com.addcel.bakery.bean.bakery.*;
import com.addcel.bakery.domain.Tarjetas;
import com.addcel.bakery.domain.Usuario;
import com.addcel.bakery.repositories.AuthorizationRepository;
import com.addcel.bakery.repositories.TarjetasRepository;
import com.addcel.bakery.repositories.UsuarioRepository;
import com.addcel.utils.AddcelCrypto;
import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PagosServiceImpl implements PagosService {

    private static final Logger LOGGER = LoggerFactory.getLogger(EnrollCardServiceImpl.class);

    @Autowired
    private PBClientService clientService;

    @Autowired
    private UsuarioRepository userRepository;

    @Autowired
    private AuthorizationRepository authorizationRepository;

    @Autowired
    private TarjetasRepository tarjetasRepository;

    @Autowired
    private EnrollCardService enrollService;

    private Gson GSON = new Gson();

    private static int SUCCESS = 200;

    @Override
    public OrdenPagoResponse generaOrdenPago(long idUsuario, double monto, String concepto,
                                             String uuid) {
        OrdenPagoResponse response = new OrdenPagoResponse();
        OrdenPagoRequest request = new OrdenPagoRequest();
        Optional<Usuario> usuario = null;
        try {
            LOGGER.info("REQUEST ORDEN PAGO: {} - MONTO - {} "+concepto, idUsuario, monto);
            request.setAmount(monto);
            request.setDescription(concepto);
            request.setCustomer_ext_id(String.valueOf(idUsuario));
            request.setSource_uuid(uuid);
            request.setTemplate_uuid(uuid);
            request.setService("2FE08235-F916-4418-B0A2-0D094D7C8742");
            request.setOperative("AUTHORIZATION");
            request.setUrl_post("https://199.231.160.203/Bakery/pb/result");
            request.setUrl_ok("https://199.231.160.203/Bakery/pb/success");
            request.setUrl_ko("https://199.231.160.203/Bakery/pb/error");

            response = clientService.generaOrdenPago(request);
        } catch (Exception e){
            e.printStackTrace();
            response.setCode(-1500);
            response.setMessage("Su transaccion no ha podido ser procesada. Codigo error: PB2500");
        }
        LOGGER.info("RESPUESTA BP: {}", GSON.toJson(response));
        return response;
    }

    @Override
    public PagoResponse realizarpago(Integer idApp, Integer idPais, String idioma, BasePayment pago) {
        PagoResponse response = new PagoResponse();
        Usuario usuario = null;
        CardResponse card = null;
        BaseResponse validation = null;
        String respBD = null;
        OrdenPagoResponse ordenPago = null;
        double total = 0;
        Transaction transaction = null;
        try{
            LOGGER.info("JSON PAGO - {}", GSON.toJson(pago));
            usuario = usuarioRegistrado(pago.getIdUser());
            if(usuario != null){
                card = enrollCard(pago.getIdCard(), String.valueOf(usuario.getIdUsuario()),
                        usuario.getNombre());
            }

            if(usuario != null && card != null){
                LOGGER.info("USUARIO OK - TARJETA ENROLLADA OK");
                respBD = authorizationRepository.validaTransaccion(pago.getIdUser(),
                        pago.getIdAplicacion(), pago.getIdioma(), pago.getIdProducto(),
                        pago.getIdCard(), pago.getImei(), "1000");
                LOGGER.info("RESPUESTA DE VALIDACION BD - {}", respBD);
                validation = GSON.fromJson(respBD, BaseResponse.class);
                if(validation.getCode() == 0){
                    //TODO INSERTAR TRANSACCION, CREAR ORDEN Y ENVIAR LA ORDEN
                    LOGGER.info("ID USUARIO: "+pago.getIdUser()+", CARGO: "+pago.getCargo()+", CONCEPTO: "+
                            pago.getConcepto()+", UUDI: "+card.getSource().getUuid());
                    respBD = authorizationRepository.insertatransaccion_payment_bakery(pago.getIdUser(), idApp, idioma,
                            1, 1, 1, pago.getConcepto(), pago.getReferencia(),
                            pago.getCargo(), pago.getComision(), pago.getIdCard(), pago.getTipoTarjeta(),
                            pago.getImei(), pago.getSoftware(), pago.getModelo(), pago.getLat(), pago.getLon());
                    transaction = GSON.fromJson(respBD, Transaction.class);
                    ordenPago = clientService.generaOrdenPago(pago.getIdUser(), pago.getCargo()+ pago.getComision(),
                            1, card.getSource().getUuid(), pago.getConcepto(), card.getSource().getUuid());

                    if(ordenPago.getCode() == SUCCESS){
                        LOGGER.info("ORDEN CREADA EXITOSAMENTE.... ENVIANDO PAGO...");
                        total = pago.getCargo() + pago.getComision();
                        /*respBD = authorizationRepository.insertatransaccion_payment_bakery(pago.getIdUser(), idApp, idioma,
                                1, 1, 1, pago.getConcepto(), pago.getReferencia(),
                                pago.getCargo(), pago.getComision(), pago.getIdCard(), pago.getTipoTarjeta(),
                                pago.getImei(), pago.getSoftware(), pago.getModelo(), pago.getLat(), pago.getLon());*/
                        LOGGER.info("RESPUESTA DE INSERTAR TRANSACCION BD - {}", respBD);
                        response = clientService.realizarPago(ordenPago.getOrder().getUuid(),
                                card.getSource().getUuid());
                    } else {
                        response.setCode(-3400);
                        response.setMessage("Su solicitud no ha podido ser procesada. Codigo error: -3400");
                    }
                } else {
                    response.setCode(-3300);
                    response.setMessage("Su solicitud no ha podido ser procesada. Codigo error: -3300");
                }
            } else {
                response.setCode(-3200);
                response.setMessage("Su solicitud no ha podido ser procesada. Codigo error: -3200");
            }
        } catch (Exception e){
            e.printStackTrace();
            response.setCode(-3100);
            response.setMessage("Su solicitud no ha podido ser procesada. Codigo error: -3100");
        }
        return response;
    }

    private Usuario usuarioRegistrado(long idUser) {
        EnrollmentResponse response = null;
        Optional<Usuario> usuario = null;
        Usuario user = null;
        try{
            usuario = userRepository.findByIdUsuario(idUser);
            if(usuario.isPresent()){
                LOGGER.info("USUARIO MOBILECARD - {} ", idUser);
                if(usuario.get().getCustomerExtId() == null){
                    user = usuario.get();
                    LOGGER.info("USUARIO NO ENROLADO - {}", user.getIdUsuario());
                    response = enrollService.enrolment(user.getIdUsuario());
                    if(response.getCode() == SUCCESS){
                        usuario.get().setCustomerExtId(response.getCustomer().getToken());
                        userRepository.save(user);
                        LOGGER.info("USUARIO ENROLADO CORRECTAMENTE");
                    }
                } else {
                    user = usuario.get();
                }
            } else {
                LOGGER.info("USUARIO NO ENCONTRADO ");
            }
        } catch (Exception e){
            e.printStackTrace();
        }
        return user;
    }

    private CardResponse enrollCard(int idCard, String customerExtid, String nombre){
        Optional<Tarjetas> cardBD = null;
        CardResponse respEnroll = null;
        try{
            cardBD = tarjetasRepository.findByIdTarjeta(idCard);
            if(cardBD.isPresent()){
                LOGGER.info("TARJETA ENCONTRADA EXITOSAMENTE ID - {}", idCard);
                if(cardBD.get().getUuid() == null ){
                    respEnroll = clientService.enrolamientoTarjeta(cardBD.get().getPan(),
                            cardBD.get().getExpDate(),
                            cardBD.get().getCt(),
                            customerExtid, nombre);
                    cardBD.get().setUuid(respEnroll.getSource().getUuid());
                    tarjetasRepository.save(cardBD.get());
//                respEnroll = clientService.enrolamientoTarjeta(cardBD.get().getPan(),
//                        cardBD.get().getExpDate(), cardBD.get().getCt(),
//                        customerExtid, nombre);
                    LOGGER.info("TARJETA HA SIDO ENROLADA CORRECTAMENTE - {}", GSON.toJson(respEnroll));
                } else {
                    LOGGER.info("TARJETA ENROLADA EN MOBILECARD - {}", GSON.toJson(cardBD.get()));
                    respEnroll = new CardResponse();
                    respEnroll.setCode(200);
                    respEnroll.setMessage("Card found in MC");
                    respEnroll.setSource(new SourceEntity());
                    respEnroll.getSource().setUuid(cardBD.get().getUuid());
                }
            }
        } catch (Exception e){
            e.printStackTrace();
        }
        return respEnroll;
    }

    @Override
    public PagoResponse depositoTarjeta(Deposito deposito) {
        PagoResponse response = null;
        try{
            response = clientService.depositoTarjeta(deposito);
        } catch (Exception e){
            e.printStackTrace();
        }
        return response;
    }

    public DevolucionPago devolucion(String orderUuid){
        Devolucion dev = new Devolucion();
        DevolucionPago responseDev = null;
        try {
            dev.setOrder_uuid(orderUuid);
            responseDev = clientService.devolucionPago(dev);
            LOGGER.info("RESPUESTA DEVOLUCION - {}", GSON.toJson(responseDev));
        } catch (Exception e){
            e.printStackTrace();
        }
        return responseDev;
    }

}
